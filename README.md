# Viêm túi mật Hiểu rõ để trị bệnh kịp thời

Viêm túi mật là tình trạng túi mật bị viêm, nhiễm trùng ở túi mật, nguyên nhân hàng đầu thường do sỏi túi mật gây ra. Nếu không điều trị kịp thời viêm túi mật có thể gây nên nhiều biến chứng nguy hiểm như phù nề, tổn thương, hoại tử thậm chí thủng tú